# Třídní sraz

Organizuje se třídní sraz **íčka** a **déčka** ročníku 1997-2001.

## Kdy
nový termín **5. září 2020** ([přidat do kalendáře](tridni-sraz.ics))

*Původní termín byl zrušen.*

## Kde
Kostelní 134, Štětí

*Vstup do školy je nyní z parku.*

---

## Účast

*Potvrzená účast k původnímu datu konání (poslední update 31.05.2020 09:32)*

    Přijede v červnu:   8 / 24
    Nedostaví se    :   4
    Klidně i v září :   11

---

#### Program
*Program bude upřesněn*

#### Ubytování
Možnost přespání na internátu (ze soboty na neděli) se chystá.

#### Kontakt
[pomykacz@odbornaskola.cz](mailto:pomykacz@odbornaskola.cz)

#### Foto školy po rekonstrukci

![Foto školy](static/school.jpg "Foto školy ptačí perspektiva")


